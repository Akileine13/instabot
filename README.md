# InstaBot 🤖

1. Télécharger [Chromium](https://www.clubic.com/telecharger-fiche432989-chromium.html) et [Node.js](https://nodejs.org/en/) (version LTS).

2. Télécharger [ici](https://gitlab.com/Akileine13/instabot/-/archive/master/instabot-master.zip) et dézipper votre dossier.

3. Dans votre dossier ```instabot-master```, lancer un terminal (Invite de commandes).

4. Installer les paquets Node.js ```npm i```.

5. Lancer le projet

| Commande  | Fonction |
| ------------- | ------------- |
| ```node bot-follow.js```  | S'abonne à liste de personne.  |

# Configuration ⚙

Dans le fichier ```config.js``` :

| Variable  | Valeur |
| ------------- | ------------- |
| ```NAME```  | Votre pseudo Instagram.  |
| ```PASSWORD```  | Votre mot de passe Instagram.  |
| ```USER```  | Utilisateur. _Exemple, [Instagram](https://instagram.com/instagram)._   |
| ```NB_FOLLOW```  | Nombre de personne suivi. _Par default, 50 personnes._  |
| ```TIMEOUT```  | Temps entre chaque follow. _Par default, 40 secondes._  |
| ```TIMER```  | Temps entre chaque action. _Par default, 3 secondes._ |
| ```DELAY```  | Vitesse d'écriture. _Par default, 0.15 secondes._ |

# Tâches 🛠

- [x] S'abonne à une liste de personne.
- [ ] Se désabonne des personnes suivies.
- [ ] Like depuis un tag.
- [ ] Like depuis son feed.

# Tip ☕

[PayPal](https://paypal.me/kloelia?locale.x=fr_FR).
