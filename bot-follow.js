'use strict';

const {NAME, PASS, USER, NB_FOLLOW, TIMEOUT, TIMER, DELAY} = require('./config');
const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({
        headless: false
    });

    const page = await browser.newPage();
    await page.setViewport({width: 1200, height: 800});
    await page.goto('https://instagram.com', {waitUntil: "networkidle2"});

    const username = 'input[name="username"]';
    const password = 'input[name="password"]';

    // Enter information username
    await page.waitForSelector(username);
    await page.type(username, NAME, {delay: DELAY});
    await page.waitFor(TIMER);

    // Enter information password
    await page.waitForSelector(password);
    await page.type(password, PASS, {delay: DELAY});
    await page.waitFor(TIMER);

    // Click button connect
    await page.waitForSelector('button[type="submit"]');
    await page.click('button[type="submit"]');
    await page.waitFor(TIMER);

    // Cancel register log
    await page.evaluate(() => {
        if (document.querySelector('.cmbtv .sqdOP') !== null) {
            document.querySelector('.cmbtv .sqdOP').click();
        }
    })

    // Popup notification
    await page.waitForSelector('.mt3GC button:last-of-type');
    await page.click('.mt3GC button:last-of-type');
    await page.waitFor(TIMER);

    // Search by user
    await page.goto('https://www.instagram.com/' + USER, {waitUntil: "networkidle2"});

    // Delete conflict button header
    await page.evaluate(() => {
        document.querySelector('.nZSzR').remove();
    })

    // Open list follower
    await page.waitForSelector('ul > .Y8-fY:nth-child(2)');
    await page.click('ul > .Y8-fY:nth-child(2)');

    await page.evaluate(async (NB_FOLLOW, TIMEOUT) => {
        await new Promise(() => {
            let i = 0;
            const follow = setInterval(async () => {
                document.getElementsByClassName('L3NKy')[i].click();
                document.querySelector('button.aOOlW.HoLwm ').click();
                document.querySelector('.isgrP').scrollTo(0, document.body.scrollHeight);

                i++;

                /*if (i % 10 === 8) {
                    document.querySelector('.isgrP').scrollTo(0, document.body.scrollHeight);
                }*/
                if (i === NB_FOLLOW) {
                    clearInterval(follow);
                }
            }, TIMEOUT);
        })
    }, NB_FOLLOW, TIMEOUT);

    await page.waitFor(TIMER);
    await browser.close();
})();